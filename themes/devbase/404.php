<?php get_header(); ?>
<div class="panel_test">
<main role="main">
	<!-- section -->
	<section class="section">
		<div class="section_content">
			<!-- article -->
			<article id="post-404">

				<h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
				</h2>

			</article>
			<!-- /article -->
		</div>
	</section>
	<!-- /section -->
</main>
</div>


<?php get_footer(); ?>
