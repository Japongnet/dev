(function ($, root, undefined) {

	$(function () {

		'use strict';

		// DOM ready, take it away

	});

})(jQuery, this);

jQuery( document ).ready(function() {
	var waypoints = jQuery('.header_toggle').waypoint({
		handler: function(direction) {
			jQuery( ".menu_section" ).toggleClass( "sticky_bottom" );

		},
		offset: 'bottom-in-view'
	});
});


jQuery( document ).ready(function() {
	var waypoints = jQuery('.header_toggle').waypoint({
		handler: function(direction) {
			jQuery( ".menu_section" ).toggleClass( "sticky" )
			jQuery( ".menu_section" ).removeClass( "sticky_bottom" )
		}
	});
});

jQuery( document ).ready(function() {

	var waypoints = jQuery('.waypoint').waypoint({

		handler: function(direction) {;

			jQuery(this.element).addClass('waypoint_loaded').trigger("swap_image");
			this.destroy();
		}

	});
});


jQuery( document ).ready(function() {

	var waypoints = jQuery('.stage_waypoint').waypoint({
		offset:"50%",
		handler: function(direction) {
			if(direction === "down") {
				jQuery('.stage_waypoint').removeClass('stage_waypoint_active');
				jQuery(this.element).addClass('stage_waypoint_active');
				stage_animate=jQuery(this.element).attr('data-alt');
				jQuery('.category_waypoint_active .project_stack_container div').removeClass().addClass(stage_animate);

			}

		}

	});
	var waypoints = jQuery('.stage_waypoint').waypoint({
		offset:"-50%",
		handler: function(direction) {
			if(direction === "up") {
				jQuery('.stage_waypoint').removeClass('stage_waypoint_active');
				jQuery(this.element).addClass('stage_waypoint_active');
				stage_animate=jQuery(this.element).attr('data-alt');
				jQuery('.category_waypoint_active .project_stack_container div').removeClass().addClass(stage_animate);
			}

		}

	});


});

jQuery( document ).ready(function() {

	var waypoints = jQuery('.waypoint').waypoint({
		offset:"25%",
		handler: function(direction) {
			if(direction === "down") {
				jQuery('.waypoint_active').removeClass('waypoint_active');
				jQuery(this.element).addClass('waypoint_active');
			}

		}

	});
	var waypoints = jQuery('.waypoint').waypoint({
		offset:"-25%",
		handler: function(direction) {
			if(direction === "up") {
				jQuery('.waypoint_active').removeClass('waypoint_active');
				jQuery(this.element).addClass('waypoint_active');
			}

		}

	});


});



jQuery( document ).ready(function() {

	var waypoints = jQuery('.category_waypoint').waypoint({
		offset:"50%",
		handler: function(direction) {

			if(direction === "down") {
				jQuery('.category_waypoint_active').removeClass('category_waypoint_active');
				jQuery(this.element).addClass('category_waypoint_active');

			}
		}

	});

	var waypoints = jQuery('.category_waypoint_bottom').waypoint({
		handler: function(direction) {
			if(direction === "up") {
				jQuery('.category_waypoint_active').removeClass('category_waypoint_active');
				jQuery(this.element).parent().addClass('category_waypoint_active');
				
			}
		}

	});
});




jQuery(document).on("swap_image", function () {
	jQuery('.waypoint_loaded img.placeholder_image').each(function () {
		var $this = jQuery(this),
		newSrc = $this.attr('data-alternate-src');
		$this.fadeOut(  function() {
			$this.attr('src', newSrc);
		});
		$this.fadeIn();
		jQuery( ".waypoint_loaded img.placeholder_image" ).removeClass( "placeholder_image" );
	});
	jQuery('.waypoint_loaded.placeholder_bg').each(function () {
		var $this = jQuery(this),
		newSrc = $this.attr('data-alternate-src');
		$this.fadeOut(  function() {

		});
		$this.fadeIn();
		jQuery( ".waypoint_loaded.placeholder_bg").removeClass( "placeholder_bg" );
	});
});

jQuery('.swap_button').click(function swap( event ){
	event.preventDefault();
	jQuery('img').each(function () {
		var $this = jQuery(this),
		newSrc = $this.attr('data-alternate-src');
		$this.fadeOut(  function() {
			$this.attr('data-alternate-src', $this.attr('src'));
			$this.attr('src', newSrc);
		});
		$this.fadeIn();
		objectFitImages();
	});
});

jQuery( document ).ready(function() {
	jQuery('.mainnav a').click(function(event) {
		event.preventDefault();
		var link = this;
		jQuery.smoothScroll({
			scrollTarget: link.hash
		});
	});
});

jQuery( document ).ready(function() {
	jQuery('a#display1').click(function(event) {
		event.preventDefault();
		jQuery('html, body').scrollable().animate({scrollTop: 0}, 400);

	});
});

jQuery( document ).ready(function() {
	jQuery('.previous').click(function(event) {
			event.preventDefault();
			jQuery.scrollify.previous();
	});
});

jQuery( document ).ready(function() {
	jQuery('.next').click(function(event) {
			event.preventDefault();
			jQuery.scrollify.next();
	});
});

jQuery( document ).ready(function() {
	jQuery(function() {
		jQuery.scrollify({
			section : ".panel_test",
			 overflowScroll: true,
			 setHeights: false,
		});
	});
});

jQuery( document ).ready(function() {
	objectFitImages();
});
