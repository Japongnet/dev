<header class="header clear">
  <?php if(is_front_page()): ?>

  <?php endif; ?>

  <div class="menu_section background_primary sticky_bottom">
    <div class="menu_content ">
      <div class="category_interior">
        Category Interior


      </div>

      <nav class="nav">


        <ul>
          <a href="<?php echo site_url(); ?>">
            <div class="header_home_logo_wide bg_contain" style="background-image:url('<?php the_field('header_home_logo_wide', 27);?>')"></div>

          </a>

        </ul>

      </nav>




    </div>
  </div>

</header>



</div>

<?php if ( is_front_page() ): ?>
  <div class="nav_sub">
    <ul class="mainnav">
      <?php

      // check if the repeater field has rows of data
      if( have_rows('page_sections_repeater') ): ?>

      <li>
        <a href="#" class="previous"><span class="fa fa-caret-up" aria-hidden="true"></span></a>

      </li>

      <?php // loop through the rows of data
      while ( have_rows('page_sections_repeater') ) : the_row(); ?>
      <li>
        <a href="#<?php the_sub_field('page_category_id'); ?>">
          <span class="nav_sub_label"><?php the_sub_field('page_category_title'); ?></span><span class="fa  fa-circle-o"></a></li>

        <?php endwhile;

        else :

          // no rows found

        endif;

        ?>
        <li>
          <a href="#" class="next"><span class="fa fa-caret-down" aria-hidden="true"></span></a>
            </li>
      </ul>


    </div>

<?php endif; ?>

    <span class="header_toggle">
    </span>
