<?php get_header(); ?>

<?php
include 'header_menu.php';
?>


  <?php
  $query = new WP_Query( array( 'post_type' => 'projects' ) );
  if ( $query->have_posts() ) {

	while ( $query->have_posts() ) {
		$query->the_post();
		include 'content_repeater.php';
	}

	/* Restore original Post Data */
	wp_reset_postdata();
} else {
	// no posts found
};



  ?>






<?php get_footer(); ?>
