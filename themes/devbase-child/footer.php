<!-- footer -->

<footer class="panel_test category_waypoint section background_dark_grey">


		<div class="footer_content row">
			<div class="col-xs-12 col-sm-6">
				<div class="section_title">
					<h2>Contact</h2>
				</div>
				<div class="footer_links">
					<!--
					<a href="<?php the_field('yelp', 27); ?>" target="_blank"><span class="fa fa-yelp">
					</span></a>
					<a href="<?php the_field('facebook', 27); ?>" target="_blank"><span class="fa fa-facebook-f">
					</span></a>
					<a href="<?php the_field('envelope', 27); ?>" target="_blank"><span class="fa	fa-envelope">
					</span></a>
					<a href="<?php the_field('coffee', 27); ?>" target="_blank"><span class="fa fa-instagram">
					</span></a>
				-->
				</div>
				<!-- copyright -->
				<div class="address">
					<?php the_field('address', 27); ?>
				</div>
				<br/>
				<p class="copyright">
					&copy; <?php echo date('Y'); ?> Copyright DSM
				</p>
			</div>




			<div class="col-xs-12 col-sm-6">
			<div id="map"></div>
			<script>
			function initMap() {
				var myLatLng = {lat: 43.6590196, lng: -79.3491235};

				var map = new google.maps.Map(document.getElementById('map'), {
					zoom: 18,
					center: myLatLng,
					styles: [
						{
							"stylers": [
								{ "weight": 0.8 },
								{ "hue": "#ffc300" },
								{ "visibility": "simplified" },
								{ "saturation": 100 },
								{ "lightness": -26 },
								{ "gamma": 0.35 }
							]
						}
					]
				});

				var marker = new google.maps.Marker({
					position: myLatLng,
					map: map,
					title: 'Sugarloaf Bakery'
				});
			}
			</script>
			<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyApDGJweG2rVI24inyMJcozht95pfhjNyU&callback=initMap"
			async defer></script>

		</div>
	</div>
		<!-- /copyright -->



</footer>
<!-- /footer -->




<?php wp_footer(); ?>

<!-- analytics -->
<script>
(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
	(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
	l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-XXXXXXXX-XX', 'yourdomain.com');
ga('send', 'pageview');
</script>

</body>
</html>
