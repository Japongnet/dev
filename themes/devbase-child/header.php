<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>


		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico?v=3" rel="shortcut icon">
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/fonts/stylesheet.css">
		<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>





	</head>
	<body <?php body_class(); ?>>



		<!-- header -->



		<!-- /header -->
