<?php get_header();

include 'header_menu.php';

 ?>

 <?php if (have_posts()): while (have_posts()) : the_post(); ?>


 	<!-- article -->


    <div class="section bg_cover <?php the_field('blog_section_class'); ?>" style="background-image:url('<?php the_field('blog_section_background');?>')">
      <div class="section_content">
        <div class="section_content <?php the_field('blog_section_orientation'); ?>">
          <div class="section_content_main ">
            <div class="section_content_text">

 		<!-- post title -->
 		<h2>
 			<a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
 		</h2>
 		<!-- /post title -->

 		<!-- post details -->
 		<span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>


 		<!-- /post details -->

 		<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>

 		<?php edit_post_link(); ?>


               <?php if( get_field('blog_section_title') ): ?>
                 <h2 class="section_title"><?php the_field('blog_section_title'); ?></h2><br/>
               <?php endif; ?>
               <?php if( get_field('blog_section_tagline') ): ?>
                 <em class="section_tagline"><?php the_field('blog_section_tagline'); ?></em>
               <?php endif; ?>
               <?php if( get_field('blog_section_text') ): ?>
                 <div class="section_text"><?php the_field('blog_section_text'); ?></div>
               <?php endif; ?>
               <?php if( get_field('blog_section_button') ): ?>
                 <button class="section_button"><?php the_field('blog_section_button'); ?></button>
               <?php endif; ?>
             </div>

           </div>


         </div>

       </div>
     </div>
 	<!-- /article -->

 <?php endwhile; ?>

 <?php else: ?>

 	<!-- article -->
 	<article>
 		<h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
 	</article>
 	<!-- /article -->

 <?php endif; ?>






 <div id="content_area">
   <!-- section -->





 </div>









<?php get_footer(); ?>
