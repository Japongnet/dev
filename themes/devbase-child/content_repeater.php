<div  class="category category_waypoint <?php the_field('project_class'); ?>">
  <div class="category_background">
  </div>

  <div class="project_stage_container">

    <!-- So we have two things going on here - every project_stage scrolls by and contains instructions for the project_stack. When the project_stage hits its waypoint, the stage's waypoints are
    sent to the project_stack.  -->
    <?php
    // check if the repeater field has rows of data
    if( have_rows('project_stages') ): ?>
    <?php // loop through the rows of data
    while ( have_rows('project_stages') ) : the_row(); ?>
    <div class="panel_test <?php the_field('project_stages_class'); ?>">
      <div class="stage">
        <span class="stage_waypoint" data-alt="<?php the_sub_field('project_stage_animation'); ?>"><?php the_sub_field('project_stage_animation'); ?>|<?php the_field('project_stages_class'); ?></span>

        <div class="stage_content">
          <h2><?php the_sub_field('project_stage_title'); ?></h2>
          <hr/>
          <?php the_sub_field('project_stage_content'); ?>
        </div>
      </div>
    </div>
  <?php endwhile; ?>
  <?php
  else :
    // no rows found
  endif;
  ?>
</div>
  <div class="project_stack_container">
    <?php
    // check if the repeater field has rows of data
    if( have_rows('project_stack') ): ?>
    <div class="stack_base">

      <?php // loop through the rows of data
      $x = 0;
      $y = 0;

      while ( have_rows('project_stack') ) : the_row();
      $y++;
      ?>
      <span class="sub_stack_base <?php the_sub_field('project_sub_stack_class'); ?> sub_stack_base_<?php echo $y ?>">
        <?php
        // check if the sub repeater field has rows of data
        if( have_rows('project_sub_stack') ): ?>

        <span class="sub_stack_base_bg">
          <?php // loop through the rows of data

          while ( have_rows('project_sub_stack') ) : the_row();

          $x++;
          ?>
          <?php if(get_sub_field('project_stack_image')){ ?>
            <img src="<?php the_sub_field('project_stack_image'); ?>" alt="" class="stack_image_<?php echo $x ?> <?php the_sub_field('project_stack_class'); ?>" />
            <?php } ?>


            <?php if(get_sub_field('project_stack_tooltip_conditional')){ ?>

              <span class="<?php the_sub_field('project_stack_class'); ?> stack_image_<?php echo $x ?>">

                <span class="<?php the_sub_field('project_stack_class'); ?>_reveal fa fa-plus-circle">
                </span>
                <span class="<?php the_sub_field('project_stack_class'); ?>_content"><?php the_sub_field('project_stack_tooltip'); ?></span>

              </span>
              <?php } ?>

            <?php endwhile; ?>
          </span>

          <?php
          else :
            // no rows found
          endif;
          ?>
        </span>
      <?php endwhile; ?>
    </div>
    <?php
    else :
      // no rows found
    endif;
    ?>
  </div>


<span class="category_waypoint_bottom"></span>
</div>
