<!-- CONTENT -->
<div class="clearfix">
  <div class="content-left">

    <h2>Can we contact you to book an appointment?</h2>

    <p>Just let us know the best way to reach you, and we’ll give you a call to set up your appointment.</p>

    <!-- BOOK APPOINTMENT FORM -->
          <script src="/mail/form.js" type="text/javascript"></script>
          <form  action="/mail/submit.php" method="post" onSubmit="return validateForm(this,'color','#FF0000','required','requiredMessage')" class="form">
          <input name="redirect" type="hidden" value="/contact.php?sent#sent" />
          <input name="subject" type="hidden" value="YorkvilleSmiles.com Website Contact Form Submission" />
        <div class="clearfix">
          <div class="left">
            <label for="book-yourname" id="rq1" > Your Name:</label>
            <input name="form[Your Name]" id="book-yourname" class="required_l-rq1" type="text" value="" />
          </div>
          <div class="right">
            <label for="book-email" id="rq2"> Your Email:</label>
            <input name="form[Email]" id="book-email" class="required_email_l-rq2" type="email" value="" />
          </div>
        </div>
        <div class="clearfix">
          <div class="left">
            <label for="book-phone" id="rq3" > Your Phone Number:</label>
            <input name="form[Phone Number]" id="book-phone" class="required_l-rq3" type="text" value="" />
          </div>
          <div class="right">
            <label for="book-verify" id="rq4"> Help Prevent Spam. 2 + 5 = ?</label>
            <input name="verification" id="book-verify" class="required_vn7_l-rq4" type="text" value="" />
          </div>
        </div>
        <button type="submit" class="learnmore" onclick="goog_report_conversion ('http://www.yorkvillesmiles.com/contact.php?sent#sent')" >Submit <img src="./images/action-arrow.png" alt="Learn More" width="10" height="10" /></button>
        <div class="error" id="requiredMessage">
          <span><strong>ERROR</strong> Please fill out all fields to continue.</span>
        </div>
      </form>

  </div>

  <!-- SIDEBAR -->
  <aside class="sidebar">

    <div class="sidebar-block facebook clearfix" style="overflow:hidden;">
      <img src="./images/sidebar-3-title.jpg" alt="Join Us on Facebook" width="159" height="62" />
      <div class="clearfix">
        <iframe src="//www.facebook.com/plugins/likebox.php?href=https%3A%2F%2Fwww.facebook.com%2Fpages%2FDr-Judy-Sturm-Dentistry-Professional-Corporation%2F188993174480938&amp;width=330&amp;height=62&amp;colorscheme=light&amp;show_faces=false&amp;border_color&amp;stream=false&amp;header=true&amp;appId=197305657056301" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:330px; height:100px;" allowTransparency="true"></iframe>
        <p>Become part of our Facebook family for regular office updates, tips for taking care of your teeth, news in dentistry and more.</p>
      </div>

    </div>

  </aside>

</div>
