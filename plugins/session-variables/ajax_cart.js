jQuery(document).ready(function($) {
	//Initial Load and Count
	jQuery(document).ready(function($) {
		$.ajax({
			url: MyAjax.ajaxurl,
			data: {
				'action':'cart_ajax_request',
			},
			success:function(data) {

				// This outputs the result of the ajax request
				jQuery( ".cart_interior" ).empty();
				jQuery( ".category_interior" ).append(data);
				//Add in the text version of the ajax content for the email
				var text_content = jQuery('.text_products').html();

				jQuery( "#wpcf7-f530-o1 #text_products_list" ).html('');
				jQuery( "#wpcf7-f530-o1 #text_products_list" ).append(text_content);

				//Add in the cart count for the nav bar
				var cart_count = jQuery( ".cart .product" ).length;
				jQuery( ".cart_count" ).empty();
				jQuery( ".cart_count" ).append(cart_count);
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
	});

	jQuery('.basket_open').click(function() {
		event.preventDefault();
		$( ".cart" ).toggleClass( "cart_open" );

	});

	jQuery('.cart').on('click', '.cart_number_check', function (){
		event.preventDefault();
		$(this).addClass('cart_checked');
	});

	//Clear the cart
	jQuery('.cart_clear').click(function() {
		var cart_clear = "cart_clear";
		event.preventDefault();
		$(".cart_checked").removeClass("cart_checked");

		$.ajax({
			url: MyAjax.ajaxurl,
			data: {
				'action':'cart_ajax_clear',
				'cart_clear' : cart_clear,
			},
			success:function(data, cart_clear) {
				// This outputs the result of the ajax request
				jQuery( ".cart_interior" ).empty();
				jQuery( ".cart_interior" ).append(data);
				var cart_count = jQuery( ".cart .product" ).length;
				jQuery( ".cart_count" ).empty();
				jQuery( ".cart_count" ).append(cart_count);
				$( ".cart" ).removeClass( "cart_open" );
			},
			error: function(errorThrown){
				console.log(errorThrown);
			}
		});
	});


	//Add items to the cart
	$(document).ajaxComplete(function(){
		jQuery('.cart_input').click(function() {

			event.preventDefault();
			var cart_id = $(this).attr('data-cart');
			if($(this).hasClass('product_delete')) {
				var cart_quantity = '0';
			} else {
				var cart_quantity = $(this).prev().children().val();
			}

			$(this).addClass('cart_checked');
			$(this).addClass('cart_updated');

			$(".basket_link").addClass( "basket_active");
			setTimeout(function () {
				$('.basket_link').removeClass('basket_active');
			}, 500);

			setTimeout(function(){
				$('.cart_updated').removeClass('cart_updated');

			},1000);




			$.ajax({
				url: MyAjax.ajaxurl,
				data: {
					'action':'cart_ajax_request',
					'cart_id' : cart_id,
					'cart_quantity': cart_quantity,

				},
				success:function(data) {

					// This outputs the result of the ajax request

					//add the ajaxed cart contents into the html containers
					jQuery( ".cart_interior" ).empty();
					jQuery( ".cart_interior" ).append(data).hide().fadeIn(250);

					//Add in the text version of the ajax content for the email
					var text_content = jQuery('.text_products').html();
					jQuery( "#wpcf7-f530-o1 #text_products_list" ).html('');
					jQuery( "#wpcf7-f530-o1 #text_products_list" ).append(text_content);

					//count the products for the nav bar
					var cart_count = jQuery( ".cart .product" ).length;
					jQuery( ".cart_count" ).empty();
					jQuery( ".cart_count" ).append(cart_count);
				},
				error: function(errorThrown){
					console.log(errorThrown);
				}
			});
		});
	});

	// We'll pass this variable to the PHP function cart_ajax_request


	// This does the ajax request


});
