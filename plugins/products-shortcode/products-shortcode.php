<?php
/*
Plugin Name: Products List Shortcode
Plugin URI: http://www.japong.net
Description: This plugin provides a shortcode to list posts, with parameters.
Version: 1.0
Author: Julian Apong
Author URI: http://www.japong.net
License: GPLv2
*/

// create shortcode to list all clothes which come in blue
// Add Shortcode
function products_list( $atts ) {


  // define attributes and their defaults
  extract( shortcode_atts( array (
  'type' => 'post',
  'posts' => -1,
  'category' => '',
  'tag' => '',
), $atts ) );

// define query parameters based on attributes
$args = array(
  'post_type' => $type,
  'order' => $order,
  'orderby' => $orderby,
  'posts_per_page' => $posts,
  'category_name' => $category,
  'tag' => $tag
);

// there is already a query running, the main query

// this is a nested secondary query
$the_query = new WP_Query( $args );
$products.='<div class="products">';
?>

<?php while( $the_query->have_posts() ) : $the_query->the_post();
// .. do some stuff


// temporarily hold the post from your secondary query
// see comment below about problem doing wp_reset_postdata();
$temp_post = $post; ?>

<?php

ob_start(); ?>

<div class="product">

  <?php if(  get_field('product_image') ): ?>
    <div class="product_image"><img class="placeholder_image" src="<?php the_field('placeholder_image', 27) ?>" data-alternate-src="<?php   $image = get_field('product_image'); echo $image['sizes']['large']; ?>" alt="<?php the_title() ?>"></div>
  <?php endif; ?>
  <div class="product_details">
    <div class="product_title"><?php the_title() ?></div>
    <span class="tag_icons">
      <?php if(has_tag('gluten-free')) {
        echo '<span class="tag_icon icon-gluten"><span class="icon_tag_text">Gluten Free</span></span>';
      }
      ?>
      <?php if(has_tag('dairy-free')) {
        echo '<span class="tag_icon icon-lactose_free"><span class="icon_tag_text">Lactose Free</span></span>';
      }
      ?>
      <?php if(has_tag('vegan')) {
        echo '<span class="tag_icon icon-vegan"><span class="icon_tag_text">Vegan</span></span>';
      }
      ?>
      <?php if(has_tag('vegetarian')) {
        echo '<span class="tag_icon icon-vegetarian"><span class="icon_tag_text">Vegatarian</span></span>';
      }
      ?>
    </span>
    <div class="product_description"><?php the_field('product_description') ?></div>
    <div class="product_price">

      <?php
      $product_price = get_field('product_price');
      echo sprintf("%01.2f", $product_price); ?>
    </div>
    <?php if(  get_field('product_for_sale') ): ?>
      <form method="post">
        <div class="cart_add"></div>
        <span class="add_to_cart">Add to Basket<span class="product_number"><input class="cart_number quantity quantity_<?php the_ID(); ?>" type="number" min="0" step="1" value="1" name="cart_number"/></span><input type="button" class="cart_input " name="Submit" value="&#xf00c;" data-cart="<?php the_ID(); ?>"/>  </span>

      </form>
    <?php endif; ?>
  </div>
</div>
<?php $products.= ob_get_contents();
ob_end_clean();

?>

<?php  $post = $temp_post;



endwhile; // end of the secondary query loop ?>

<?php
$products.='</div>';
// this resets post data to main query
wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly
return $products;

}
add_shortcode( 'products-list', 'products_list' );
?>
